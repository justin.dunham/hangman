const fetch = require("node-fetch");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
class Hangman {
  word;
  guesses = [];
  maxGuesses;

  constructor(props = {}) {
    this.maxGuesses = props?.maxGuesses || 5;
    this.getNewWord();
  }

  async getNewWord() {
    this.guesses = [];
    const getWord = await fetch("https://puzzle.mead.io/puzzle?wordCount=1")
      .then((res) => res.json())
      .catch((ex) => ({
        puzzle: "badconnection",
      }));
    this.word = getWord?.puzzle?.toLowerCase() || "";
    return this.word;
  }

  checkWord() {
    const { guesses } = this;
    return this.word
      .toLowerCase()
      .split("")
      .map((letter) => (guesses.includes(letter) ? letter : "_"))
      .join("");
  }

  prompt() {
    rl.question("What is your guess? ", (guess) => {
      this.turn(guess);
    });
  }

  turn(guess = "") {
    console.clear();

    if (!this.word) {
      setTimeout(() => {
        this.turn();
      }, 300);
      return false;
    }

    this.guesses.push(guess);

    if (guess.toLowerCase() == this.word.toLowerCase()) {
      console.log(`You Win! The word was : ${this.word}`);
      setTimeout(() => {
        this.newGame();
      }, 5000);
      return false;
    }

    if (this.word.toLowerCase().split("").includes(guess)) {
      if (this.checkWord() == this.word) {
        console.log(`You Win! The word was : ${this.word}`);
        setTimeout(() => {
          this.newGame();
        }, 5000);
        return false;
      }
      console.log(`Correct Letter!`);
    }

    console.log(`Your word is: ${this.checkWord()}`);

    if (this.guesses.length >= this.maxGuesses) {
      ("You lose! :(");
    }

    this.prompt();
  }

  async newGame() {
    this.word = "";

    this.getNewWord();
    this.turn();
  }
}

module.exports = Hangman;
