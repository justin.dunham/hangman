const Hangman = require("./Hangman.js");

describe("Hangman", () => {
  let hangman;
  let puzzleWord;
  beforeAll(async () => {
    hangman = new Hangman();
    puzzleWord = await hangman.getNewWord();
  });

  it("picks new word", async () => {
    expect(puzzleWord.length).toBeGreaterThan(0);
  });

  it("Displays puzzled word", async () => {
    const dashes = new Array(puzzleWord.length).fill("_").join("");

    global.console.log = jest.fn();
    hangman.turn();

    expect(console.log).toBeCalledWith(`Your word is: ${dashes}`);
  });

  it("adds correct letters", async () => {
    global.console.log = jest.fn();
    hangman.turn(puzzleWord[1]);

    expect(console.log.mock.calls).toEqual([
      [`Correct Letter!`],
      [`Your word is: ${hangman.checkWord()}`],
    ]);
  });

  it("wins on correct word guess", async () => {
    global.console.log = jest.fn();
    hangman.turn(puzzleWord);

    expect(console.log).toBeCalledWith(`You Win! The word was : ${puzzleWord}`);
  });

  it("picks nose", async () => {});

  it("picks nose", async () => {});
});
